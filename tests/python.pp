$version = '3.6.0'
$versions = [$version]
$venv = '/tmp/snafu'
$pkg = 'hw'

class {'python':
  user              => 'root',
  group             => 'root',
  bashrc            => '/root/.bashrc',
  pyenv_install_dir => '/root/.pyenv',
  versions          => $versions
}
-> python::virtualenv {$venv:
  python_version =>  $version
}
-> python::pip {'pgadmin':
  python_version => $version,
  virtualenv     => $venv,
  install_dir    => $venv,
  pkgname        => $pkg

}
