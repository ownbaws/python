$version = '3.6.0'
$versions = [$version, '2.7.7']
$venv = '/tmp/snafu'
$pkg = 'pgadmin4'

class {'python':
  python_versions => $versions,
  pip_url         => $url,
}
-> python::virtualenv {$venv:
  python_version =>  $version
}
-> python::pip {'pgadmin':
  python_version => $version,
  virtualenv     => $venv,
  install_dir    => $venv,
  pkgname        => $pkg

}
