require 'spec_helper'
describe 'python::virtualenv' do
  context 'python::virtualenv' do
    let(:title ) { 'virtual_env' }
    let(:params) do
      {
        'ensure'         => 'present',
        'python_version' => '2.7.5',
        'user'           => 'foo',
        'group'          => 'bar',
        'runas'          => 'foo',
        'pyenv_root'     => '/tmp/pyenv'
      }
    end
    let(:facts) do
      {
        'osfamily'                  => 'RedHat',
        'operatingsystem'           => 'RedHat',
        'path'                      => '/usr/local/bin:/bin:/usr/bin:/usr/X11R6/bin:/opt/puppetlabs/bin',
        'operatingsystemmajrelease' => '7',
      }
    end
    it { should contain_virtualenv('virtual_env').with(
      'ensure'         => 'present',
      'python_version' => '2.7.5',
      'user'           => 'foo',
      'group'          => 'bar',
      'runas'          => 'foo',
      'pyenv_root'     => '/tmp/pyenv'
    )}
  end
end

