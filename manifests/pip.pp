# == Class: python::package
# Author: Siebren Zwerver
#

define python::pip (
  String $python_version,
  String $pkgname    = $title,
  String $user       = $python::user,
  String $group      = $python::group,
  String $runas      = $user,
  String $pyenv_root = $python::pyenv_install_dir,
  Optional[String] $install_dir    = undef,
  Optional[String] $requirements   = undef,
  Optional[String] $virtualenv     = undef,
) {
  pip {$title:
    pkgname        => $pkgname,
    user           => $user,
    group          => $group,
    runas          => $runas,
    requirements   => $requirements,
    virtualenv     => $virtualenv,
    install_dir    => $virtualenv,
    python_version => $python_version,
    pyenv_root     => $pyenv_root
  }
}
