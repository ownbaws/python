require 'spec_helper'
describe 'python' do
  context 'init' do
    let(:params) do
      {
        :python_versions => ['2.7.5']
      }
    end
    let(:facts) do
      {
        'osfamily'                  => 'RedHat',
        'operatingsystem'           => 'RedHat',
        'path'                      => '/usr/local/bin:/bin:/usr/bin:/usr/X11R6/bin:/opt/puppetlabs/bin',
        'operatingsystemmajrelease' => '7',
      }
    end
    it { should contain_class('python') }
    it { should contain_class('python::package') }
    it { should contain_python__pyenv('2.7.5') }


    it { should contain_file('/etc/pip.conf') \
      .with_content(/.*pypi.*/)
    }
  end
end
