# == Class: python
# Author: Siebren Zwerver
#

class python (
  Enum['present','absent']       $pyenv_ensure,
  Boolean                        $virtualenv_ensure,
  String                         $pyenv_version,
  String                         $pyenv_url,
  String                         $user,
  String                         $group,
  String                         $bashrc,
  String                         $pyenv_install_dir,
  String                         $pip_url,
  Optional[Variant[Array, Hash]] $versions = undef,
) {
  class {'python::package':
    pyenv_ensure  => $pyenv_ensure,
    pyenv_version => $pyenv_version,
    url           => $pyenv_url,
    user          => $user,
    group         => $group,
    install_dir   => $pyenv_install_dir,
    before        => File['/etc/pip.conf']
  }
  file { '/etc/pip.conf':
    mode    => '0644',
    content => "[global]\nindex-url = ${pip_url}\nindex = ${pip_url}\n"
  }

  class {'python::config': require => Class['python::package']}

  if $versions {
    $versions.each|$version, $params| {
      if $version =~ Integer {
        $real_version = $params
        $real_params = {
          ensure      => present,
          virtualenv  => true,
          version     => $params,
          runas       => $user,
          install_dir => $pyenv_install_dir
        }
      }
      else {
        $real_version = $version
        $real_params = $params
      }

      python::pyenv { $real_version:
        *       => $real_params,
        require => Class['python::package']
      }
    }
  }
}
