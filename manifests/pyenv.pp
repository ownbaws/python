# == Define: python::pyenv
#
define python::pyenv (
  Enum['present','absent'] $ensure,
  String                   $version,
  String                   $runas,
  String                   $install_dir,
  Boolean                  $virtualenv,
) {
  pyenv {$version:
    ensure      => $ensure,
    runas       => $runas,
    install_dir => $install_dir
  }

  if $virtualenv == true {
    python::pip {"pyenv-${version}-virtualenv":
      pkgname        => 'virtualenv',
      python_version => $version
    }
  }
}
