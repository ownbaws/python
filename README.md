# Python

#### Table of contents
1. [Overview](#markdown-header-overview)
2. [Module Description - What the module does and why it is useful](#markdown-header-module-description)
3. [Usage - Configuration options and additional functionality](#markdown-header-usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#markdown-header-reference)
    * [Class: python ](#markdown-header-class-python)
    * [Class: python::pip ](#markdown-header-class-pythonpip)
    * [Class: python::virtualenv ](#markdown-header-class-pythonvirtutalenv)
5. [Limitations](#markdown-header-limitations)

# Overview
This python module allows you to easely manage multiple python versions for a single user

# Module Description
The python module provides a single module to manage `pyenv`, `pip` and `virtualenv` for a single user. And completely ignores the system installed python.

# Usage
To install pyenv and configure pip automatically to your local pip mirror.

```puppet
class {'python':
  python_versions => '1.2.3',
  pip_url        => 'https://url/to/pip',
}
```

To enable virtualenv for all versions installed you only have to specify `virtualenv => true`.

```puppet
class {'python':
  versions => ['2.7.5','2.7.6']
  pip_url         => 'https://url/for/pip',
}
-> python::virtualenv {'/tmp/foobar':
  python_version => '2.7.5'
}
-> python::pip {'pgadmin':
  python_version => '2.7.5',
  virtualenv     => '/tmp/foobar',
  install_dir    => '/tmp/foobar',
  pkgname        => 'pgadmin4'

}
```

This can also be set through hiera:

```yaml
python::versions:
  - '2.7.5'
  - '2.7.6'
  - '2.7.7'
python::virtualenv: true
python::pip_url: 'https://path/to/pip'
```

or specify an `hash` of versions

```yaml
python::versions:
  '2.7.5':
    ensure: absent
  '2.7.6':
    ensure: present
    virtualenv: false
  '2.7.7':
    ensure: present
    virtualenv: true
```

With the above hiera values you can just specify
```puppet
class {'python': }
```

# Reference
## class: python
* `pyenv_ensure` If the specified version should be present.
  * Values: `present` `absent`.
  * Default `present`
* `virtualenv_ensure` If virtual env should be installed default.
  * Values `true` `false`.
  * Default `true`
* `pyenv_version` Which pyenv version should be installed.
  * Values `String`.
  * Default `1.2.5`
* `pyenv_url` Where to get pyenv from.
  * Values `String`.
  * Default `https://github.com/pyenv/pyenv.git`
* `user` The user pyenv should be installed for.
  * Values `String`.
  * Default `ec2-user`
* `group` The group pyenv should be installed for.
  * Values `String`.
  * Default `ec2-user`
* `pyenv_install_dir` Where to install pyenv.
  * Values: `String`.
  * Default `/home/$user/.pyenv`
* `pip_url` The pip url.
  * Values: `String`.
  * Default `https://pypi.org/simple`
* `python_versions` The python versions that should be installed.
  * Values `Hash` or `Array`.
  * Default `empty`

## class: python::config
* `bashrc` The bash rc of the user
  * Value: `String`
  * Default: `/home/${hiera(python::user)}/.bashrc`
* `ensure` If the pyenv settings should be present.
  * Values `present` `absent`.
  * Default `present`
* `user` The user of the bashrc
  * Values: `String`.
  * Default `$python::user`
* `install_dir` Where pyenv is installed.
  * Value `String`.
  * Default `$python::pyenv_install_dir`

## class: python::virtualenv
* `ensure` If the virtualenv should be present.
  * Values `present` `absent`.
  * Default `present`
* `python_version` Which python version to use.
  * Required
  * Value `String`
* `user` The user pyenv is installed for.
  * Values: `String`.
  * Default `$python::user`
* `group` The group pyenv is installed for.
  * Values: `String`.
  * Default `$python::group`
* `runas` The user that runs the commnad.
  * Values `String`.
  * Default `$user`
* `virtualenv` The virtualenv name.
  * Values `String`.
  * Default `$title`
* `pyenv_root` Where pyenv is installed.
  * Values `String`.
  * Default `$python::pyenv_install_dir`


## class: python::pip
* `install_dir` Where to install the package.
  * Required
  * Value `String`
* `python_version` Which python version to use.
  * Required
  * Value `String`
* `pkgname` The package name.
  * Value `String`.
  * Default `$title`
* `user` The owner of the package.
  * Value `String`.
  * Default `$python::user`
* `group` The owner group of the package.
  * Value `String`.
  * Default `$python::group`
* `runas` The user pip should un as.
  * Value `String`.
  * Default `$user`
* `pyenv_root` Where pyenv is installed.
  * Value `String`.
  * Default `$python::pyenv_install_dir`
* `requirements` To install the requirements.
  * Value `String`.
  * Default `undef`
* `virtualenv` Which virtualenv to use.
  * Value `String`.
  * Default `undef`

## Limitations

Only *NIX os like systems can use this module.
